#!/usr/bin/python3.7

from flask import Flask
from flask import render_template
from api import Api
from htmlSupport.jsonworker import JsonWorker

app = Flask(__name__)


@app.route('/')
def endpoint_index():
    return render_template('base.html', dbOptions=Api().getVaildDbs())


@app.route('/ajax/getChart/<int:start_year>/<int:end_year>/<kWord>/<db>')
def endpoint_getChartData(start_year, end_year, kWord, db):

    apiObj = Api()
    json = JsonWorker()

    if db not in apiObj.getVaildDbs():
        return json.objectToString({"sucess": False, "err": "DB invalid"})

    if start_year < 1944:
        return json.objectToString({
            "sucess": False,
            "err": "start year is before 1944"
        })

    if end_year < 1944:
        return json.objectToString({
            "sucess": False,
            "err": "end year is before 1944"
        })

    chartData = apiObj.getRange(
        startYear=start_year,
        endYear=end_year,
        keyword=kWord,
        db=db
    )

    return json.objectToString({
        "sucess": True,
        "data": chartData
    })


if __name__ == '__main__':
    app.run()
