#!/usr/bin/python3.7

from json import dumps, loads


class JsonWorker_Exception(Exception):
    pass


class JsonWorker:

    def stringToObject(self, obj: str):
        """ converts a JSON string to a python dict"""

        if not isinstance(obj, str):
            raise JsonWorker_Exception('the passed object should be a str')

        return loads(obj)

    def objectToString(self, obj: dict):
        """ converts a dict to a Json String"""

        if not isinstance(obj, dict):
            raise JsonWorker_Exception('the passed object should be a dict')

        return dumps(obj)
