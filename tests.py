from unittest import main
from api.tests import Db_tests, Data_tests

Db_tests()
Data_tests()

if __name__ == "__main__":
    main()
