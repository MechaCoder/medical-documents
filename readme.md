# medical-documents

## installation

the control of the dependencies for the project, are controlled using in `pipenv` and using `python3.7` this is written using `flask` as the web framework and also making use. for a server, i would recommend using (Ubuntu)[https://www.ubuntu.com/]

### checking prerequisites

1) check that you have python 3.7 installed you can do this by checking in typing in `python3.7` into your terminal if you see a response that is `command not found: python3.7` then go to the (CPython website)[https://docs.python.org/3/using/index.html] for installation, Windows users should also make sure that the python is added to your  system path as the installer does not always do this.

2) pip also needs to be installed to check this you need to do this you can by putting pip3 into your command line `pip3 -V` (with a Capital V), you can install pip3 install on windows (read this)[https://vgkits.org/blog/pip3-windows-howto/] and for Linux (read this)[https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/]

### installing depencys

1) you can now install all the dep `pip install -r requirements` once this is done you can try to run the applcation

## runing the App

run the command `python3.7 app.py` to run the command and then click (here)[http://127.0.0.1:5000/]