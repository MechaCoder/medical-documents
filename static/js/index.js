
function App () {
  var self = {}

  var bind = function () {
    $('#getChart').on('click', function () {
      var data = self.getInfoForAjax()

      var path = '' + '/ajax/getChart/' + data['startDate'] + '/' + data['endDate'] + '/' + data['keyword'] + '/' + data['db']
      var responseData = self.makeCall(path) // the request object for Ajax call.
      var processedObject = JSON.parse(responseData.responseText)
      var years = Object.keys(processedObject.data)

      if (!processedObject.sucess) {
        console.error(processedObject)
        alert('there has been an error with getting the data for the chart, sorry')
        return false
      }

      var values = []
      for (var i = 0; i <= years.length; i++) {
        values.push(processedObject.data[years[i]])
      }

      self.makeChart(
        years,
        values
      )
    })
  }

  var main = function () {
    self.getInfoForAjax = function () {
      // get all infromation needed to make an Ajax call
      var returnobj = {}

      returnobj['startDate'] = $('select#startDate option:selected').attr('value')
      returnobj['endDate'] = $('select#endDate option:selected').attr('value')
      returnobj['keyword'] = $('#keyword').val()
      returnobj['db'] = $('select#dbs option:selected').attr('value')

      return returnobj
    }

    self.makeCall = function (url) {
      var requestObject = $.ajax({
        url: url,
        async: false
      })

      return requestObject
    }

    self.makeChart = function (datalabels, dataItems) {
      var ctx = document.getElementById('myChart')
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: datalabels,
          datasets: [{
            label: '# number of documents',
            data: dataItems,
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      })
    }
  }

  var init = function () {
    main()
    bind()
  }
  init()
}

App()
