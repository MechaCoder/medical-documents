#!/usr/bin/python3.7

from tinydb import TinyDB
from tinydb import Query


class Database:

    def __init__(self, fileLocalation: str='dataSource.json'):
        self.fileLocalation = fileLocalation

    def create(self, year: str, value: int, keyword: str, data: str):

        dataBaseObk = TinyDB(self.fileLocalation)
        table = dataBaseObk.table('year')

        docid = table.insert({
            "year": year,
            "value": value,
            "keyword": keyword,
            "database": data
        })

        dataBaseObk.close()

        return docid

    def contains(self, year: str, kword: str, dbase: str):

        dataBaseObk = TinyDB(self.fileLocalation)
        table = dataBaseObk.table('year')

        query = Query()

        contained = table.contains(
            (query.year == year) &
            (query.keyword == kword) &
            (query.database == dbase)
        )
        dataBaseObk.close()

        return contained

    def getValue(self, year: str, keyword: str, dbase: str):

        dataBaseObk = TinyDB(self.fileLocalation)
        table = dataBaseObk.table('year')

        query = Query()

        rows = table.search(
            (query.year == year) &
            (query.keyword == keyword) &
            (query.database == dbase)
        )

        dataBaseObk.close()

        row = rows[0]
        return row['value']
