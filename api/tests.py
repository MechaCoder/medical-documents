from unittest import TestCase, main

from api.db import Database
from api.data import Api_base


class Db_tests(TestCase):

    def setUp(self):

        self.fileLocal = './datatestDB.json'

    def test_init(self):

        obj = Database(self.fileLocal)

        self.assertEqual(
            obj.fileLocalation,
            self.fileLocal
        )

    def test_create(self):

        obj = Database(self.fileLocal)
        rowid = obj.create(
            '1944',
            45,
            'keyword',
            'data'
        )

        self.assertIsInstance(
            rowid,
            int
        )

    def test_contains(self):

        obj = Database(self.fileLocal)
        self.assertIsInstance(
            obj.contains(
                1944,
                'keyword',
                'data'
            ),
            bool
        )

    def test_getValue(self):
        obj = Database(self.fileLocal)
        self.assertIsInstance(
            obj.getValue(
                '1944',
                'keyword',
                'data'
            ),
            int
        )


class Data_tests(TestCase):

    def test_getDbList(self):

        obj = Api_base().getDbList()
        self.assertIsInstance(
            obj,
            list
        )

    def test_getCountByYearAndKeyword(self):
        obj = Api_base().getCountByYearAndKeyword(
            1945,
            'canser'
        )

        self.assertIsInstance(
            obj,
            int
        )


if __name__ == '__main__':
    main()
