from requests import get
from requests.models import Response


class Api_base_exception(Exception):
    pass


class Api_base:

    def _urlBuild(self, api: str='einfo'):
        # esearch
        return f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/{api}.fcgi'

    def _revewResponse(self, obj: Response):
        """ tests a response object"""

        if isinstance(obj, Response) is False:
            # tests the the object type is right
            raise Api_base_exception(
                'Resopnse Object is not `requests.models.Response`'
            )

        if obj.status_code != 200:
            # is the response okay
            raise Api_base_exception(
                f'Resopnse.status_code is not 200 - {obj.status_code}'
            )

    def getDbList(self):
        """ returns a list of the all acessable dbs """

        resp = get(self._urlBuild() + '?retmode=json')
        self._revewResponse(resp)
        jsonobj = resp.json()

        if 'einforesult' not in jsonobj.keys():
            raise Api_base_exception(
                f'einforesult has not been found'
            )
        if 'dblist' not in jsonobj['einforesult'].keys():
            raise Api_base_exception(
                f'einforsult.dblist has not been found'
            )
        return jsonobj['einforesult']['dblist']

    def getCountByYearAndKeyword(
        self,
        year: int,
        keyword: str,
        db: str='pubmed'
    ):
        """ returns a interger from the api that is the total amount
            of rows that are held by year, keyword and db
        """

        addr = f'?db={db}&term={keyword}&mindate={year}&maxdate={year}'
        addr += '&retmode=json&usehistory=y'
        resp = get(self._urlBuild('esearch') + addr)
        self._revewResponse(resp)
        jsonobj = resp.json()

        if 'esearchresult' not in jsonobj.keys():
            raise Api_base_exception(
                f'esearchresult has not been found'
            )

        if 'count' not in jsonobj['esearchresult'].keys():
            raise Api_base_exception(
                f'esearchresult.count has not been found'
            )
        return int(jsonobj['esearchresult']['count'])
