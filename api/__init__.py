from api.data import Api_base
from api.db import Database


class Api_Exception(Exception):
    pass


class Api(Api_base):

    def __init__(self):
        self.dbWorker = Database()

    def getRange(
        self,
        startYear: int,
        endYear: int,
        keyword: str,
        db: str='pubmed'
    ):
        """ returns dict of all counts beween startYear and endYear """

        if isinstance(startYear, int) is False:
            raise Api_Exception('The startYear should be an int')

        if isinstance(endYear, int) is False:
            raise Api_Exception('the endYear should be an int')

        strStartYear = str(startYear)
        strEndYear = str(endYear)

        if len(strStartYear) != 4:
            raise Api_Exception('the startYear should be made up 4 ints')

        if len(strEndYear) != 4:
            Api_Exception('the endYear should be made up 4 ints')

        apiRange = range(startYear, endYear)

        returnObj = {}
        for year in apiRange:

            if self.dbWorker.contains(year, kword=keyword, dbase=db):
                # do i have that data already?
                returnObj[year] = self.dbWorker.getValue(
                    year=year,
                    keyword=keyword,
                    dbase=db
                )
                continue

            count = self.getCountByYearAndKeyword(
                year=year,
                keyword=keyword,
                db=db
            )
            returnObj[year] = count

            # adds this to the db
            self.dbWorker.create(
                year=year,
                value=count,
                keyword=keyword,
                data=db
            )

        return returnObj

    def getVaildDbs(self):
        """ gets all the vaild ids form the api"""
        return self.getDbList()
